<?php
/**
 * Add the question code to class
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2015 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 1.0.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class addQuestionCodeClass extends  \ls\pluginmanager\PluginBase
{
  protected $storage = 'DbStorage';

  static protected $name = 'addQuestionCodeClass';
  static protected $description = 'Add the question code to the class of the question wrapper.';

  protected $settings = array(
    'addQuestionCode' => array(
      'type' => 'select',
      'label' => 'Add the question code to all question in all survey (by default)',
      'options'=>array(
        0=> 'No',
        1=> 'Yes'
      ),
      'default'=>1,
    ),
    'strtolowerQuestionCode' => array(
      'type' => 'select',
      'label' => 'Converts Question code to lowercase',
      'options'=>array(
        0=> 'No',
        1=> 'Yes'
      ),
      'default'=>1,
    ),
  );

  /**
  * Subscribe to events 
  */
  public function init()
  {
    $this->subscribe('beforeQuestionRender','addQuestionCode');
    $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');
  }
  /**
  * Add the settings for the survey
  */
  public function beforeSurveySettings()
  {
    $oEvent = $this->event;
    $oEvent->set("surveysettings.{$this->id}", array(
      'name' => get_class($this),
      'settings' => array(
      'addQuestionCode' => array(
        'type' => 'select',
        'label' => 'Add the question code to all question in all survey (by default)',
        'options'=>array(
          0=> 'No',
          1=> 'Yes'
        ),
        'current'=>$this->get('addQuestionCode','Survey',$oEvent->get('survey'),$this->get('addQuestionCode',null,null,$this->settings['addQuestionCode']['default'])),
      ),
      'strtolowerQuestionCode' => array(
        'type' => 'select',
        'label' => 'Converts Question code to lowercase',
        'options'=>array(
          0=> 'No',
          1=> 'Yes'
        ),
        'current'=>$this->get('strtolowerQuestionCode','Survey',$oEvent->get('survey'),$this->get('strtolowerQuestionCode',null,null,$this->settings['addQuestionCode']['default'])),
      ),
      ),
    ));
  }
  /**
  * Save the settings
  */
  public function newSurveySettings()
  {
    $oEvent = $this->event;
    foreach ($oEvent->get('settings') as $name => $value)
    {
      $this->set($name, $value, 'Survey', $event->get('survey'));
    }
  }

  /**
  * Add the question code to the class according to : Survey settings, else global settings, else defult setting
  */
  public function addQuestionCode()
  {
    $oEvent=$this->getEvent();
    if(!$this->get('addQuestionCode','Survey',$oEvent->get('surveyId'),$this->get('addQuestionCode',null,null,$this->settings['addQuestionCode']['default'])))
      return;
    $sCode=$oEvent->get('code');
    if($this->get('strtolowerQuestionCode','Survey',$oEvent->get('surveyId'),$this->get('strtolowerQuestionCode',null,null,$this->settings['strtolowerQuestionCode']['default'])))
      $sCode=strtolower($sCode);
    $oEvent->set('class',$oEvent->get('class')." $sCode");
  }
}
