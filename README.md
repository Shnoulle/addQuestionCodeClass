# addQuestionCodeClass

A plugin for LimeSurvey to add the question code to the questions classes

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06 only)
- Clone in plugins/addQuestionCodeClass directory

## Home page & Copyright
- HomePage <http://wwww.sondages.pro/>
- Copyright © 2015 Denis Chenu <http://sondages.pro>

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/gpl.txt) licence
